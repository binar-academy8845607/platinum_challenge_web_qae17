<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Elektronik</name>
   <tag></tag>
   <elementGuidId>a4967bf7-5a61-4c21-a438-932f0a6a219a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@href = '/?category_id=4']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='besi kotor'])[1]/preceding::a[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/?category_id=4</value>
      <webElementGuid>d6ae70a4-3a4f-4397-b2f8-796d90abc337</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        
        Elektronik
</value>
      <webElementGuid>5fbecc92-63c6-45a9-aeec-f88361acd3a3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='besi kotor'])[1]/preceding::a[2]</value>
      <webElementGuid>490b8028-95bd-4f9f-8252-f964057c6883</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Elektronik']/parent::*</value>
      <webElementGuid>529fe7f6-e8a6-402b-ad5c-6026800aa841</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/?category_id=4')]</value>
      <webElementGuid>0167cf35-c96c-4549-bc9f-8590c0d45698</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[5]</value>
      <webElementGuid>d5f7f0f3-7569-421b-b20f-6d183365c43d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/?category_id=4' and (text() = '
        
        Elektronik
' or . = '
        
        Elektronik
')]</value>
      <webElementGuid>aaa86178-b652-40b9-a220-cfe1b9820c58</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
