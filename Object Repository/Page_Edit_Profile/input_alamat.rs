<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_alamat</name>
   <tag></tag>
   <elementGuidId>bff9619b-e001-4cec-bbdd-8e9c5427aaa6</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#user_address</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@placeholder = 'Contoh: Jalan Ikan Hiu 33' and @name = 'user[address]' and @id = 'user_address' and (text() = 'ok' or . = 'ok')]</value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='user_address']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>9efbb96b-95c5-400b-abea-507b4fc26039</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control rounded-4 p-3</value>
      <webElementGuid>5ce83fbb-4362-4e29-ab06-5c8ca8c193f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Contoh: Jalan Ikan Hiu 33</value>
      <webElementGuid>373ea667-eb58-4c93-b779-831c889720d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>required</name>
      <type>Main</type>
      <value>required</value>
      <webElementGuid>112001f5-cbc4-47b5-8ff9-a039031ed137</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>user[address]</value>
      <webElementGuid>f63e02bf-3488-46d4-b9a9-e88cf1861a66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>user_address</value>
      <webElementGuid>4876b7a2-848b-4960-936f-dd0a8cbe3249</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ok</value>
      <webElementGuid>5e24fe0f-38d1-4af7-85d4-d9c4b7d626e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;user_address&quot;)</value>
      <webElementGuid>9293c160-a005-4b9b-8b30-10639dcb5439</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='user_address']</value>
      <webElementGuid>f407ab82-f5fc-48c8-949e-ee47d30650a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alamat'])[1]/following::textarea[1]</value>
      <webElementGuid>b0aa197d-d206-4859-b532-d7820754400b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kota'])[1]/following::textarea[1]</value>
      <webElementGuid>95db1a37-2a37-4cad-8774-54cc2a1ad473</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No Handphone'])[1]/preceding::textarea[1]</value>
      <webElementGuid>3f9b3fc2-ccb3-4047-bbb8-7d967d62b67a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='ok']/parent::*</value>
      <webElementGuid>296e0330-f875-4c37-afe6-a152863e1a1a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>d3ed20bf-7075-413c-bdbe-67c16092cbbb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@placeholder = 'Contoh: Jalan Ikan Hiu 33' and @name = 'user[address]' and @id = 'user_address' and (text() = 'ok' or . = 'ok')]</value>
      <webElementGuid>0db5f0ba-e284-4582-a3f0-a979dafba2f2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
