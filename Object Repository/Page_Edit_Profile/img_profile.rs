<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_profile</name>
   <tag></tag>
   <elementGuidId>ef6cfaee-8828-4aad-9fa7-0f4d7c68e19e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.d-flex.align-items-center.justify-content.center.flex-column.py-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbarSupportedContent']/div/ul/li[6]/ul/li/a/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a2949ba4-1122-4371-b00e-10d8c0d33205</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>d-flex align-items-center justify-content center flex-column py-2</value>
      <webElementGuid>2449e371-bd84-49d9-914d-f300cd6b5464</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              

            
              liza
              Jogja
            
          </value>
      <webElementGuid>b7e3f24c-234a-490d-9ff5-d35bcad25432</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbarSupportedContent&quot;)/div[@class=&quot;ms-auto&quot;]/ul[@class=&quot;navbar-nav&quot;]/li[@class=&quot;nav-item dropdown fs-5 d-none d-lg-block&quot;]/ul[@class=&quot;dropdown-menu show&quot;]/li[1]/a[@class=&quot;nav-user&quot;]/div[@class=&quot;d-flex align-items-center justify-content center flex-column py-2&quot;]</value>
      <webElementGuid>18707fa4-0a85-4eb9-bfd3-2a3f0727a4e9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbarSupportedContent']/div/ul/li[6]/ul/li/a/div</value>
      <webElementGuid>d9156e7f-1fa6-4e8d-89dd-c405f58cdfe3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profil Saya'])[2]/following::div[1]</value>
      <webElementGuid>1b3e57ce-53f1-4686-9161-9b3938bf798a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Keluar'])[1]/following::div[1]</value>
      <webElementGuid>340c0c3e-c8a0-472f-8ae7-e764ea9b3cdb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/ul/li/a/div</value>
      <webElementGuid>a54e52f7-3ce5-4a8e-85fc-2fb1515446db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
              

            
              liza
              Jogja
            
          ' or . = '
              

            
              liza
              Jogja
            
          ')]</value>
      <webElementGuid>1b7620ce-865f-433e-bc92-6430aec37e04</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
