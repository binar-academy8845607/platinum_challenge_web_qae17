<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>notif_Invalid Email or password</name>
   <tag></tag>
   <elementGuidId>10fb477c-d560-4da3-87fe-e2ef82f78c70</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/preceding::div[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '
        Invalid Email or password.
        
      ' or . = '
        Invalid Email or password.
        
      ')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-danger.alert-dismissible.position-fixed.top-0.start-50.translate-middle-x.mt-5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>16f72a9f-8bee-4286-a7c2-d33b2fa71386</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-danger alert-dismissible position-fixed top-0 start-50 translate-middle-x mt-5</value>
      <webElementGuid>976ef3dc-f05c-4702-abbc-09e43310013f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>alert</value>
      <webElementGuid>67cce25f-069a-46cd-981b-c0a47fe1fec0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Invalid Email or password.
        
      </value>
      <webElementGuid>8af85837-cb76-4d67-aff1-f811d8d02d7f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;alert alert-danger alert-dismissible position-fixed top-0 start-50 translate-middle-x mt-5&quot;]</value>
      <webElementGuid>6afd5399-2a25-44f8-95ad-7eb080bac035</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/preceding::div[1]</value>
      <webElementGuid>fce251a7-de3c-4f0c-a27e-c0aaf7ea8b83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk'])[1]/preceding::div[2]</value>
      <webElementGuid>0fb61614-f7cd-4343-92e1-e4187443c68e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Invalid Email or password.']/parent::*</value>
      <webElementGuid>e0bb86b2-b951-49b6-82ba-5f5c1a65e17c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div</value>
      <webElementGuid>ee1323e6-2f34-47e0-be06-b0a966c36113</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Invalid Email or password.
        
      ' or . = '
        Invalid Email or password.
        
      ')]</value>
      <webElementGuid>5e1b7b04-339f-420f-873c-eccf1f74dc21</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
