<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>form_Perbarui status penjualan</name>
   <tag></tag>
   <elementGuidId>4bdae8ba-7296-4828-9f21-5a3fe7a14fa1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>form.modal-content.px-4.py-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@action='/offers/89831']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
      <webElementGuid>eaf1825f-a347-43f0-9756-e3cac88aff75</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-content px-4 py-2</value>
      <webElementGuid>eb398ff0-80f2-4042-b147-3bb28aaa0c1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>/offers/89831</value>
      <webElementGuid>20229880-33a1-4865-a070-638158f6d83f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>accept-charset</name>
      <type>Main</type>
      <value>UTF-8</value>
      <webElementGuid>73e963c0-0522-4441-9864-11ee09a7cc61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>method</name>
      <type>Main</type>
      <value>post</value>
      <webElementGuid>cbc5082a-26e3-4119-b20c-a50db1a47e2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            
              
            

            
              Perbarui status penjualan produkmu
              
                
                
                  Berhasil terjual
                Kamu telah sepakat menjual produk ini kepada pembeli
              
              
                
                
                  Batalkan transaksi
                Kamu membatalkan transaksi produk ini dengan pembeli
              
            

            
              
            
</value>
      <webElementGuid>ba045b4a-ff41-4fda-bf24-6c86dc62f9c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;statusModal89831&quot;)/div[@class=&quot;modal-dialog modal-dialog-centered&quot;]/form[@class=&quot;modal-content px-4 py-2&quot;]</value>
      <webElementGuid>710b53b7-7581-470c-8b5a-1ec227a10bf1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@action='/offers/89831']</value>
      <webElementGuid>f119bdcd-4c7c-43f4-8a17-7f2a2d618e4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='statusModal89831']/div/form</value>
      <webElementGuid>bd485299-0b11-4dc2-94c9-5df4ac89308a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hubungi di'])[1]/following::form[1]</value>
      <webElementGuid>b67158dd-08b5-46d2-8cf2-32eab518864d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Status'])[1]/following::form[1]</value>
      <webElementGuid>19af71eb-7362-4d28-aa61-86a7f7fa0ba7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form</value>
      <webElementGuid>381335c6-8356-4993-9aa9-be3f2ad78413</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//form[(text() = '
            
              
            

            
              Perbarui status penjualan produkmu
              
                
                
                  Berhasil terjual
                Kamu telah sepakat menjual produk ini kepada pembeli
              
              
                
                
                  Batalkan transaksi
                Kamu membatalkan transaksi produk ini dengan pembeli
              
            

            
              
            
' or . = '
            
              
            

            
              Perbarui status penjualan produkmu
              
                
                
                  Berhasil terjual
                Kamu telah sepakat menjual produk ini kepada pembeli
              
              
                
                
                  Batalkan transaksi
                Kamu membatalkan transaksi produk ini dengan pembeli
              
            

            
              
            
')]</value>
      <webElementGuid>c6e1e67e-1844-439b-8297-93b926679d32</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
