<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_hapeRp 1.000.000Ditawar Rp 800.000</name>
   <tag></tag>
   <elementGuidId>e595513e-52ed-44c3-8dec-83d38ab29fe2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.p-5.notification-list > li > a.notification.my-4.px-2.position-relative > div.notification-body.me-4 > div.notification-body.fs-5</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'hapeRp 1.000.000Ditawar Rp 800.000' or . = 'hapeRp 1.000.000Ditawar Rp 800.000')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[5]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e72de6cc-b1cd-45dc-8fef-466eb014e97a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notification-body fs-5</value>
      <webElementGuid>9ada8608-0ef3-48c3-b848-0f1138604ba8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>hapeRp 1.000.000Ditawar Rp 800.000</value>
      <webElementGuid>a01a8a34-2904-43a7-b059-015ad1b20823</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]/ul[@class=&quot;p-5 notification-list&quot;]/li[1]/a[@class=&quot;notification my-4 px-2 position-relative&quot;]/div[@class=&quot;notification-body me-4&quot;]/div[@class=&quot;notification-body fs-5&quot;]</value>
      <webElementGuid>36c74ae7-cf87-4af9-b525-f2ca82e393a8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[5]/following::div[1]</value>
      <webElementGuid>43bcc4a9-67f7-422d-9fdb-6d1041d3d08b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New alerts'])[13]/preceding::div[2]</value>
      <webElementGuid>772c8c09-6c6a-4526-8b48-b29a0dc3ae9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[6]/preceding::div[2]</value>
      <webElementGuid>2b4bdaac-e31f-4e7c-bfde-039c940b1cb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul/li/a/div/div[2]</value>
      <webElementGuid>78a1df2f-9d1f-4bb0-bb30-0b44f19940cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'hapeRp 1.000.000Ditawar Rp 800.000' or . = 'hapeRp 1.000.000Ditawar Rp 800.000')]</value>
      <webElementGuid>e05c487c-e2f8-4c1e-a7a8-ce83d07957e1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
