<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_hapeRp 1.000.000Ditawar Rp 300.000.000</name>
   <tag></tag>
   <elementGuidId>78e6224c-f6b2-4dfb-9c03-68cbd721debc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.p-5.notification-list > li:nth-of-type(3) > a.notification.my-4.px-2.position-relative > div.notification-body.me-4 > div.notification-body.fs-5</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'hapeRp 1.000.000Ditawar Rp 300.000.000' or . = 'hapeRp 1.000.000Ditawar Rp 300.000.000')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[6]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3947bd0a-6c81-4c25-9969-eb11c31a01d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notification-body fs-5</value>
      <webElementGuid>859f6538-8a73-4ccf-9066-40e0981f152b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>hapeRp 1.000.000Ditawar Rp 300.000.000</value>
      <webElementGuid>f07fe8e0-d9bd-4b17-a57c-d1e97f90385a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]/ul[@class=&quot;p-5 notification-list&quot;]/li[3]/a[@class=&quot;notification my-4 px-2 position-relative&quot;]/div[@class=&quot;notification-body me-4&quot;]/div[@class=&quot;notification-body fs-5&quot;]</value>
      <webElementGuid>3bd78daf-6fe1-494c-a748-4c8acb2bd44a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[6]/following::div[1]</value>
      <webElementGuid>89afd61f-eb34-4055-ab75-707c3e37988c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New alerts'])[13]/following::div[3]</value>
      <webElementGuid>05f738a2-e69d-42ac-9507-3e7a99d49e40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New alerts'])[14]/preceding::div[2]</value>
      <webElementGuid>223c7d75-1bf6-474b-b7c5-a65873a3ce57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[7]/preceding::div[2]</value>
      <webElementGuid>64a03a19-ce5b-4e11-9cc9-10387b404253</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul/li[3]/a/div/div[2]</value>
      <webElementGuid>478ca11e-b6f4-455b-aeb7-a17b7cd729b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'hapeRp 1.000.000Ditawar Rp 300.000.000' or . = 'hapeRp 1.000.000Ditawar Rp 300.000.000')]</value>
      <webElementGuid>619d96ee-53e3-4090-929d-2f83c21a1618</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
