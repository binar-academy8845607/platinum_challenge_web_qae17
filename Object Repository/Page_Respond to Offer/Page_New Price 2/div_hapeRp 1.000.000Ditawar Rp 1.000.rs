<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_hapeRp 1.000.000Ditawar Rp 1.000</name>
   <tag></tag>
   <elementGuidId>cc4cbb78-e0df-4329-b444-a3edf032795c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.p-5.notification-list > li:nth-of-type(5) > a.notification.my-4.px-2.position-relative > div.notification-body.me-4 > div.notification-body.fs-5</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'hapeRp 1.000.000Ditawar Rp 1.000' or . = 'hapeRp 1.000.000Ditawar Rp 1.000')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[7]/following::div[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d014a902-0163-486d-81d1-e0766b18b007</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notification-body fs-5</value>
      <webElementGuid>fb4d7aa2-a3fc-4884-b0d4-ea5ef85e4cee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>hapeRp 1.000.000Ditawar Rp 1.000</value>
      <webElementGuid>9843818c-c42a-4597-b073-e9b69810cdcb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;pt-5 mt-5&quot;]/section[@class=&quot;container-sm&quot;]/ul[@class=&quot;p-5 notification-list&quot;]/li[5]/a[@class=&quot;notification my-4 px-2 position-relative&quot;]/div[@class=&quot;notification-body me-4&quot;]/div[@class=&quot;notification-body fs-5&quot;]</value>
      <webElementGuid>34f50b33-eedb-4683-a84f-a741e8d25d89</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[7]/following::div[1]</value>
      <webElementGuid>1742e222-78b1-44bf-94df-ad6eed0f1923</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New alerts'])[14]/following::div[3]</value>
      <webElementGuid>a441c987-3142-44df-a73b-cdc70e019cd6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New alerts'])[15]/preceding::div[2]</value>
      <webElementGuid>5d526a16-405e-41f3-9228-81c977d6eb61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penawaran produk'])[8]/preceding::div[2]</value>
      <webElementGuid>1775b9d3-117d-4722-98b9-52770056f3f0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/ul/li[5]/a/div/div[2]</value>
      <webElementGuid>85d6293b-7cb9-4895-a1aa-baa987c45e4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'hapeRp 1.000.000Ditawar Rp 1.000' or . = 'hapeRp 1.000.000Ditawar Rp 1.000')]</value>
      <webElementGuid>66937354-04b8-4d08-8392-ad26f1e2af94</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
