Feature: Respond to Offer

	Scenario: User want to respond accept product offer with normal price
		Given User on notification page of product offer with normal price
		When User accept product offer with normal price
		Then User on What's App Page
		
	Scenario: User want to reject product offer
		Given User on notification page of product offer to reject
		When User reject product offer
		Then User on notification page
		
	Scenario: User want to accept product offer with negative price
		Given User on notification page of product offer with negative price
		When User accept product offer with negative price
		Then User notification page with negative price
		
	Scenario: User want to accept product offer with higher price
		Given User on notification page of product offer with higher price
		When User accept product offer
		Then User notification page with higher price