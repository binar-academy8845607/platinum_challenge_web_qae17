package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

class RespondtoOffer {

	@Given("User on notification page of product offer with normal price")
	def navigateToNotificationPage() {

		WebUI.openBrowser('')

		WebUI.maximizeWindow()

		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')

		WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/a_Masuk'))

		WebUI.setText(findTestObject('Object Repository/Page_Respond to Offer/input_Email_useremail'), 'imanjuwita@gmail.com')

		WebUI.setEncryptedText(findTestObject('Object Repository/Page_Respond to Offer/input_Password_userpassword'),
				'OGLiSTAGMFx0LAJ/xjX7tg==')

		WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/input_Password_commit'))

		WebUI.click(findTestObject('Page_Respond to Offer/i_New alerts_bell icon'))

		WebUI.click(findTestObject('Page_Respond to Offer/a_Lihat semua notifikasi'))
	}

	@When("User accept product offer with normal price")
	def clickAccept() {
		WebUI.click(findTestObject('Page_Respond to Offer/Page_New Price 3/div_hapeRp 1.000.000Ditawar Rp 800.000'))

		WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/label_Terima'))

		WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/a_Hubungi di WA'))

		WebUI.switchToWindowTitle('Share on WhatsApp')
	}

	@Then("User on What's App Page")
	def verifyWhatsApp() {
		WebUI.verifyElementVisible(findTestObject('Page_Respond to Offer/Page_Share on WhatsApp/img_wa'))
		WebUI.closeBrowser()
	}

	@Given ("User on notification page of product offer to reject")
	def navigateToNotificationRejectPage() {

		WebUI.openBrowser('')

		WebUI.maximizeWindow()

		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')

		WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/a_Masuk'))

		WebUI.setText(findTestObject('Object Repository/Page_Respond to Offer/input_Email_useremail'), 'imanjuwita@gmail.com')

		WebUI.setEncryptedText(findTestObject('Object Repository/Page_Respond to Offer/input_Password_userpassword'),
				'OGLiSTAGMFx0LAJ/xjX7tg==')

		WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/input_Password_commit'))

		WebUI.click(findTestObject('Page_Respond to Offer/i_New alerts_bell icon'))

		WebUI.click(findTestObject('Page_Respond to Offer/a_Lihat semua notifikasi'))
	}


	@When ("User reject product offer")
	def clickReject() {

		WebUI.click(findTestObject('Page_Respond to Offer/Page_New Price 3/div_hapeRp 1.000.000Ditawar Rp 1.000'))

		WebUI.click(findTestObject('Page_Respond to Offer/Page_Tolak Tawaran/label_Tolak'))
	}

	@Then ("User on notification page")
	def verifyReject() {
		WebUI.verifyElementVisible(findTestObject('Page_Respond to Offer/Page_Tolak Tawaran/Verify_Daftar Produkmu yang Ditawar'))

		WebUI.closeBrowser()
	}

	@Given ("User on notification page of product offer with negative price")
	def navigateToNotificationNegative() {
		WebUI.openBrowser('')

		WebUI.maximizeWindow()

		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')

		WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/a_Masuk'))

		WebUI.setText(findTestObject('Object Repository/Page_Respond to Offer/input_Email_useremail'), 'imanjuwita@gmail.com')

		WebUI.setEncryptedText(findTestObject('Object Repository/Page_Respond to Offer/input_Password_userpassword'),
				'OGLiSTAGMFx0LAJ/xjX7tg==')

		WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/input_Password_commit'))

		WebUI.click(findTestObject('Page_Respond to Offer/i_New alerts_bell icon'))

		WebUI.click(findTestObject('Page_Respond to Offer/a_Lihat semua notifikasi'))
	}

	@When ("User accept product offer with negative price")
	def clickAcceptNegative() {

		WebUI.click(findTestObject('Page_Respond to Offer/Page_New Price 3/div_hapeRp 1.000.000Ditawar Rp -100'))

		WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/label_Terima'))

		WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/a_Hubungi di WA'))

		WebUI.switchToWindowTitle('Share on WhatsApp')
	}

	@Then ("User notification page with negative price")
	def verifyNegative() {
		WebUI.verifyElementVisible(findTestObject('Page_Respond to Offer/Page_Share on WhatsApp/img_wa'))

		WebUI.closeBrowser()
	}
	@Given ("User on notification page of product offer with higher price")
	def navigateToNotificationPageHiherPrice() {
		WebUI.openBrowser('')

		WebUI.maximizeWindow()

		WebUI.navigateToUrl('https://secondhand.binaracademy.org/')

		WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/a_Masuk'))

		WebUI.setText(findTestObject('Object Repository/Page_Respond to Offer/input_Email_useremail'), 'imanjuwita@gmail.com')

		WebUI.setEncryptedText(findTestObject('Object Repository/Page_Respond to Offer/input_Password_userpassword'),
				'OGLiSTAGMFx0LAJ/xjX7tg==')

		WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/input_Password_commit'))

		WebUI.click(findTestObject('Page_Respond to Offer/i_New alerts_bell icon'))

		WebUI.click(findTestObject('Page_Respond to Offer/a_Lihat semua notifikasi'))
	}
	@When ("User accept product offer")
	def clickAcceptHigherPrice() {


		WebUI.click(findTestObject('Page_Respond to Offer/Page_New Price 3/div_hapeRp 1.000.000Ditawar Rp 300.000.000'))

		WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/label_Terima'))

		WebUI.click(findTestObject('Object Repository/Page_Respond to Offer/a_Hubungi di WA'))

		WebUI.switchToWindowTitle('Share on WhatsApp')
	}

	@Then ("User notification page with higher price")
	def verifyHigherPrice() {
		WebUI.verifyElementVisible(findTestObject('Page_Respond to Offer/Page_Share on WhatsApp/img_wa'))

		WebUI.closeBrowser()
	}
}