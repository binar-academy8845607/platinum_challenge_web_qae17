$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("D:/Binar Academy/Platinum/platinum_challenge_web_qae17/Include/features/negotiable.feature");
formatter.feature({
  "name": "negotiable",
  "description": "  I want to use this template for my feature file",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@tag"
    }
  ]
});
formatter.scenario({
  "name": "the user wants to buy the product",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@tag"
    },
    {
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "name": "user negotiate goods",
  "keyword": "Given "
});
formatter.match({
  "location": "negotiable.user_negotiate()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user enter bid price and click send",
  "keyword": "When "
});
formatter.match({
  "location": "negotiable.enter_bid_price()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "offer send",
  "keyword": "Then "
});
formatter.match({
  "location": "negotiable.request_send()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "The user does not log in first",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@tag"
    }
  ]
});
formatter.step({
  "name": "users search for items",
  "keyword": "Given "
});
formatter.match({
  "location": "negotiable.search_item()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user selects items",
  "keyword": "When "
});
formatter.match({
  "location": "negotiable.select_item()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user must log in",
  "keyword": "Then "
});
formatter.match({
  "location": "negotiable.must_login()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "users bid at a minus price",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@tag"
    }
  ]
});
formatter.step({
  "name": "the user wants to bid on the desired item",
  "keyword": "Given "
});
formatter.match({
  "location": "negotiable.bid_desired_item()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is interested in one item",
  "keyword": "When "
});
formatter.match({
  "location": "negotiable.interested_item()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Waiting for the seller\u0027s answer",
  "keyword": "Then "
});
formatter.match({
  "location": "negotiable.waiting_seller()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "make an offer with an invalid email",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@tag"
    }
  ]
});
formatter.step({
  "name": "users are interested in the items being sold",
  "keyword": "Given "
});
formatter.match({
  "location": "negotiable.item_being_sold()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user enters email",
  "keyword": "When "
});
formatter.match({
  "location": "negotiable.enter_email()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "invalid user email",
  "keyword": "Then "
});
formatter.match({
  "location": "negotiable.invalid_email()"
});
formatter.result({
  "status": "passed"
});
});